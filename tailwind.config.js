/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      white: "#FFFFFF",
      black: "#000000",
      gray: {
        dark: "#111827",
        light: "#D1D5DB",
        DEFAULT: "#111827",
      },
      blue: "#3B82F6",
      teal: "#0D9488",
      green: "#16A34A",
    },
    fontFamily: {
      sans: ["Inter", "sans-serif"],
    },
    extend: {},
  },
  plugins: [],
};
