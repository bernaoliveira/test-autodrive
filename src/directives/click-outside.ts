import type { Directive } from "vue";

export const ClickOutside: Directive = {
  mounted(el, binding, vnode) {
    let clicks = 0;
    el.clickOutsideEvent = function (event: Event) {
      if (!(el === event.target || el.contains(event.target)) && clicks > 0) {
        binding.value(event, el);
      }
      clicks++;
    };
    document.body.addEventListener("click", el.clickOutsideEvent);
  },
  unmounted(el) {
    document.body.removeEventListener("click", el.clickOutsideEvent);
  },
};
