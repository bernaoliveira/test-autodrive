import { createApp } from "vue";
// @ts-ignore
import Maska from "maska";

import App from "./App.vue";
import store from "./store/index";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";

import { ClickOutside } from "@/directives/click-outside";

import "./assets/main.css";

const app = createApp(App);

app.use(router);
app.use(store);

app.use(VueAxios, axios);
app.provide("axios", app.config.globalProperties.axios);

app.use(Maska);

app.directive("click-outside", ClickOutside);

app.mount("#app");
