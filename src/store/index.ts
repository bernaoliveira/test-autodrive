import { createStore } from "vuex";
import { City } from "@/store/enums/city.enum";

export default createStore({
  state() {
    return {
      popupShown: false,
      activeCity: City.Moscow,
    };
  },
  mutations: {
    showPopup(state, city: City) {
      state.popupShown = true;
      state.activeCity = city;
    },
    closePopup(state) {
      state.popupShown = false;
    },
  },
});
